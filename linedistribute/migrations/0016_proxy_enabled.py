# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0015_siteconfiguration_initial_proxy'),
    ]

    operations = [
        migrations.AddField(
            model_name='proxy',
            name='enabled',
            field=models.BooleanField(default=True),
        ),
    ]
