# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0002_auto_20150501_2149'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='filter_active_contact',
            field=models.BooleanField(default=False, help_text='Filter only active contact'),
            preserve_default=True,
        ),
    ]
