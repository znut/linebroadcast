# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0005_siteconfiguration_notification_sound'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='chat_font',
            field=models.PositiveIntegerField(default=12, help_text='Font size in chat message (px)'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='siteconfiguration',
            name='notification_sound',
            field=models.CharField(default='ohoh', choices=[('ohoh', 'Oh-Oh'), ('ohoh2', 'Oh-Oh2'), ('guitar', 'Guitar'), ('firstblood', 'First blood'), ('doublekill', 'Double kill'), ('killingspree', 'Killing Spree'), ('dominating', 'Dominating'), ('megakill', 'Mega kill'), ('unstoppable', 'Unstoppable'), ('wickedstick', 'Wicked sick'), ('monsterkill', 'Monster kill'), ('godlike', 'Godlike'), ('holyshit', 'Holyshit'), ('cs1', 'cs1'), ('cs2', 'cs2'), ('cs3', 'cs3'), ('cs4', 'cs4'), ('cs5', 'cs5')], max_length=255, blank=True, help_text='Notification sound in index page (null for mute).', null=True),
            preserve_default=True,
        ),
    ]
