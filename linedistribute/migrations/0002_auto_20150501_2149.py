# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='siteconfiguration',
            name='auto_reload',
            field=models.PositiveIntegerField(default=10, help_text='Auto reload in seconds (set null for not reload)', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='siteconfiguration',
            name='contact_limit',
            field=models.PositiveIntegerField(default=None, help_text='Limit contacts (set null for unlimit)', null=True, blank=True),
            preserve_default=True,
        ),
    ]
