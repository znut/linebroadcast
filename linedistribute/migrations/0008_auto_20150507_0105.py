# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('linedistribute', '0007_auto_20150505_0255'),
    ]

    operations = [
        migrations.AlterField(
            model_name='siteconfiguration',
            name='notification_sound',
            field=models.CharField(default='line_low', choices=[('line_low', 'line_low'), ('line_high', 'line_high'), ('line_bell', 'line_bell'), ('ohoh', 'Oh-Oh'), ('ohoh2', 'Oh-Oh2'), ('guitar', 'Guitar'), ('firstblood', 'First blood'), ('doublekill', 'Double kill'), ('killingspree', 'Killing Spree'), ('dominating', 'Dominating'), ('megakill', 'Mega kill'), ('unstoppable', 'Unstoppable'), ('wickedstick', 'Wicked sick'), ('monsterkill', 'Monster kill'), ('godlike', 'Godlike'), ('holyshit', 'Holyshit'), ('cs1', 'cs1'), ('cs2', 'cs2'), ('cs3', 'cs3'), ('cs4', 'cs4'), ('cs5', 'cs5')], max_length=255, blank=True, help_text='Notification sound in index page (null for mute).', null=True),
            preserve_default=True,
        ),
    ]
