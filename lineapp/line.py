# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from ssl import SSLError
import traceback
import datetime
import logging
import string
import socket
import errno
import time
import json
import re

from django.contrib.auth.models import User
from django.db import transaction

from lineapi import LineClient, LineGroup, LineContact, LineRoom
from linedistribute.models import SiteConfiguration
from curve.ttypes import ContentType, OperationType
from .models import Contact, Task, Message
from .line_command import LineCommand
import lineapi


REQUIRE_API_VERSION = (0, 4, 0)
assert hasattr(lineapi, 'VERSION'), 'LineAPI version too old.'
assert lineapi.VERSION >= REQUIRE_API_VERSION, 'LineAPI version older than %s' % ('.'.join(map(str, REQUIRE_API_VERSION)))


class Line(LineClient, LineCommand):

    def __init__(self, account):
        self.config = SiteConfiguration.get_solo()
        print "Log-in ..............",
        line_pc_name       = self.config.line_pc_name
        self.account       = account
        # self.certificate   = account.certificate
        if self.account.auth_token:
            super(Line, self).__init__(account.email, account.password, authToken=self.account.auth_token, is_mac=False, com_name=line_pc_name, revision=account.revision)
        else:
            super(Line, self).__init__(account.email, account.password, is_mac=False, com_name=line_pc_name, revision=account.revision)
        self.account.auth_token = self.authToken
        self.account.save(update_fields=['auth_token'])  # save token
        print "successed"
        self.message_queue = []
        self.last_updated  = time.time()
        self.logger_name   = 'line'
        self.is_run        = True
        self.logger        = logging.getLogger(self.logger_name)

        account.profile = self.profile.to_model(account)
        account.save()  # Save profile

        print "Getting groups ......",
        time.sleep(1)
        with transaction.atomic():
            self.refreshGroups()
            groups = [x for x in self.groups]
            [x.to_model(account, force_update_member=True) for x in groups]
            print "successed"
        print "Getting contacts ....",
        time.sleep(1)
        with transaction.atomic():
            self.refreshContacts()
            contacts = [x for x in self.contacts]
            [x.to_model(account) for x in contacts]
            print "successed"
        print "Getting rooms .......",
        time.sleep(1)
        self.refreshActiveRooms()
        with transaction.atomic():
            rooms = [x for x in self.rooms]
            [x.to_model(account) for x in rooms]
            print "successed"

        print 'latest revision %s' % self._login_revision
        print 'now revision    %s' % self.revision

    @property
    def certificate(self):
        if not hasattr(self, '_certificate'):
            self._certificate = self.account.certificate
        return self._certificate

    @certificate.setter
    def certificate(self, value):
        if not hasattr(self, '_certificate'):
            self._certificate = self.account.certificate
        if self._certificate != value:
            self._certificate = value
            self.account.certificate = self._certificate
            self.account.save(update_fields=['certificate'])

    def process_message(self, message):
        msg        = message.text
        sender     = message.sender
        receiver   = message.receiver

        '''
        Save contact
        '''
        contact_sender = None
        if sender:
            if isinstance(sender, LineContact) or isinstance(sender, LineGroup) or isinstance(sender, LineRoom):
                contact_sender = sender.to_model(self.account)
        if receiver:
            if isinstance(receiver, LineContact) or isinstance(receiver, LineGroup) or isinstance(receiver, LineRoom):
                receiver = receiver.to_model(self.account)

        if not message.sender and message.sender_id != self.account.profile.lineid:
            # หา sender ไม่เจอ
            # มีสองกรณีคือ ไม่เคยแอด กับ เคยแอดแต่ลบไป
            if self.config.auto_add_contact and message.receiver_id.startswith('u'):
                try:
                    sender = message.sender = self.findAndAddContactsByMid(message.sender_id)  # Auto add contact
                    sender.to_model(self.account)
                except Exception as e:  # sender_id maybe deleted
                    print 'Sender %s not found (%s), skipped.' % (message.sender_id, e)

            contact_sender = self.account.contacts.filter(lineid=message.sender_id).first()
            if not contact_sender:
                # กรณีไม่เคยแอดเลย ลองหาดูอีกรอบ
                message.sender = self.getContactOrRoomOrGroupById(message.sender_id)
                if not message.sender:
                    print 'Sender %s not found, skipped.' % message.sender_id
                    return
                contact_sender = message.sender.to_model(self.account)
            else:
                # กรณีเคยแอดแล้ว แต่ลบไป
                pass

        if not contact_sender:
            contact_sender = Contact.objects.filter(lineid=message.sender_id, account=self.account).first()  # อาจจะเป็น Profile ตัวเอง
        if self.revision >= self._login_revision and msg:  # สรุปว่าถ้ามี และเป็นข้อความใหม่ ให้นำไปเข้าบอท
            # if isinstance(contact_sender, Contact):  # Sender ไม่น่าจะเป็น Type อื่นได้นอกจาก Contact
            from linebot.models import BotRole, BotMessageHistory
            roles = BotRole.get_roles(contact_sender, receiver)
            for role in roles:
                if not msg.startswith(role.activate_word):
                    continue
                api_message = msg[len(role.activate_word):]
                if BotMessageHistory.objects.filter(lineid=message.id, botrole=role).exists():  # ถ้าเป็นข้อความจากบอทเอง ให้ ignore
                    continue
                result = role.call_api(api_message, sender=contact_sender)
                if result.success:
                    if message.receiver_id != self.account.profile.lineid:
                        target_id = message.receiver_id
                    else:
                        target_id = message.sender_id
                    bot_message_id = self.send_message(target_id, result.message)
                    bot_message_history = BotMessageHistory(lineid=bot_message_id, botrole=role, sender=contact_sender, text=result.message)
                    bot_message_history.save()
                else:
                    self.logger.warning('Bot error: %s.' % result.message)

        if not message.receiver and message.receiver_id != self.account.profile.lineid:
                receiver = self.account.contacts.filter(lineid=message.receiver_id).first()
                if not receiver:
                    if message.receiver_id.startswith('r'):  # Maybe new Room
                        message.receiver = self.getContactOrRoomOrGroupById(message.receiver_id, True)
                        if not message.receiver:
                            from lineapi.curve.ttypes import Room as ThriftRoom  # Create room
                            ThriftRoom(mid=message.receiver_id, contacts=[message.sender])
                    if message.receiver_id.startswith('g') or message.receiver_id.startswith('c'):  # Maybe new Group
                        message.receiver = self.getContactOrRoomOrGroupById(message.receiver_id, True)
                        if not message.receiver:
                            from lineapi.curve.ttypes import Group as ThriftGroup  # Create room
                            ThriftGroup(mid=message.receiver_id, contacts=[message.sender])
                    else:
                        if self.config.auto_add_contact:
                            try:
                                message.receiver = self.findAndAddContactsByMid(message.receiver_id)
                            except Exception as e:
                                print 'Receiver %s not found (%s), skipped.' % (message.receiver_id, e)
                        else:
                            message.receiver = self.getContactOrRoomOrGroupById(message.receiver_id)
                    if not message.receiver:
                        print 'Receiver not found, skipped.'
                        return
                    message.receiver.to_model(self.account)
                else:
                    message.receiver = receiver  # Type Contact (Django Model)

        if message.contentType == ContentType.STICKER:
            # e.g. http://dl.stickershop.line.naver.jp/products/0/0/100/3/PC/stickers.zip
            # sticker_ver = message.contentMetadata['STKVER']
            # sticker_package_id = message.contentMetadata['STKPKGID']
            # sticker_id = message.contentMetadata['STKID']
            # sticker_url = 'http://dl.stickershop.line.naver.jp/products/0/0/%s/%s/PC/stickers/%s.png' % (sticker_ver, sticker_package_id, sticker_id)
            # send_to.sendSticker(stickerId=message.contentMetadata['STKID'], stickerPackageId=message.contentMetadata['STKPKGID'], stickerVersion=message.contentMetadata['STKVER'], stickerText=message.contentMetadata['STKTXT'])
            message.to_model(self.account)
        elif message.contentType == ContentType.IMAGE:
            message.to_model(self.account)
        elif message.contentType == ContentType.VIDEO:
            pass
        elif msg:  # if is text
            message.to_model(self.account)  # Save message
        elif not msg:
            print 'ContentType = %s' % message.contentType
            # print message.contentMetadata
            print 'Null msg detected.'
            return

    def _long_poll(self, long_poll_generator):
        self.account.update_line()
        try:
            while True:
                operation_type, operation = long_poll_generator.next()
                if(
                    operation_type == OperationType.RECEIVE_MESSAGE
                    or operation_type == OperationType.SEND_MESSAGE
                ):
                    self.process_message(operation)
                elif(operation_type == OperationType.NOTIFIED_READ_MESSAGE):
                    message_id = operation.param3
                    message = Message.objects.filter(lineid=message_id).first()
                    if message:
                        message.mark_read()
                    else:
                        print 'Line message not found to READ'
                elif(operation_type == OperationType.RECEIVE_MESSAGE_RECEIPT):
                    messages = operation.param2.split('\x1e')
                    for message_id in messages:
                        message = Message.objects.filter(lineid=message_id).first()
                        if message:
                            message.mark_read()
                        else:
                            print 'Line message not found to READ'
                else:
                    raise Exception('Unhandled Exception OperationType: %s' % operation_type)
        except StopIteration:
            pass

    def long_poll(self):
        '''
        Individual
            sender=Contact, receiver=Contact(You)
            Meaning: Contact chat to You

            sender=Contact(You), receiver=Contact
            Meaning: You chat to Contact
        Group
            sender=Contact, receiver=LineGroup
            Meaning: Contact chat to Group
        '''
        try:
            client = self
            long_poll_count = 50
            if self.revision >= self._login_revision:
                self.do_task()  # Do only when finished
            else:  # Load large
                long_poll_count = 1000
            time.sleep(1)
            long_poll_generator = client.longPoll(long_poll_count)
            if self.revision >= self._login_revision:  # Not use transaction.atomic cuz make MySQL gone away?
                self._long_poll(long_poll_generator)
            else:
                with transaction.atomic():
                    self._long_poll(long_poll_generator)
        except SSLError as e:
            self.logger.warning('SSLError (%s), continue.' % e)

    def do_task(self):
        while True:
            task = Task.objects.filter(completed=False).first()
            if not task:
                return
            time.sleep(1)  # Prevent Line Banned
            print '[^] %s' % (task.action.upper())
            try:
                result = getattr(self, task.action)(**task.args)
                task.set_complete(result=result)
            except TypeError as e:
                print 'TypeError: %s' % e
                traceback.print_exc()

    def run(self):
        try:
            # client = self.client
            client = self
            self.logger.info('Logged in as %s' % client.profile.name)
            while True:
                if not self.is_run:  # Stop run
                    print 'Stopped run.'
                    break
                self.last_updated = time.time()
                self.long_poll()
                time.sleep(0.5)
        except KeyboardInterrupt:
            self.logger.info('Keyboard Interrupt.')
            return
        # except socket.error as error:
        #     if error.errno == getattr(errno, 'WSAECONNRESET', None):
        #         self.logger.info('Handle connection reset.')
        #         raise
        #     else:
        #         self.logger.info('Connection error (%s).' % error)
        #         raise

    @property
    def revision(self):
        return self._revision

    @revision.setter
    def revision(self, val):
        self._revision = val
        if self.account.revision == val:
            return
        self.account.revision = val
        self.account.save(update_fields=['revision'])
