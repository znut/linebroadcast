# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0030_auto_20150531_1415'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='latest_line_read_message',
            field=models.ForeignKey(related_name='line_read_contacts', blank=True, to='lineapp.Message', null=True),
        ),
        migrations.AddField(
            model_name='group',
            name='latest_line_read_message',
            field=models.ForeignKey(related_name='line_read_groups', blank=True, to='lineapp.Message', null=True),
        ),
        migrations.AddField(
            model_name='room',
            name='latest_line_read_message',
            field=models.ForeignKey(related_name='line_read_rooms', blank=True, to='lineapp.Message', null=True),
        ),
    ]
