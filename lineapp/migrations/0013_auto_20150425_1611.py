# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0012_account_revision'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='link_contact',
            field=models.ForeignKey(related_name='messages', blank=True, to='lineapp.Contact', null=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='link_group',
            field=models.ForeignKey(related_name='messages', blank=True, to='lineapp.Group', null=True),
            preserve_default=True,
        ),
    ]
