# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0025_account_updated_on'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message'), ('find_contact_by_userid', 'Find contact by userid'), ('add_contact_by_userid', 'Add contact by userid'), ('get_blocked_contacts', 'Get blocked contacts')]),
            preserve_default=True,
        ),
    ]
