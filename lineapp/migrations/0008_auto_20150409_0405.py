# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0007_task'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='message_updated_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='group',
            name='account',
            field=models.ForeignKey(related_name='group_group', default=None, to='lineapp.Account'),
            preserve_default=False,
        ),
        migrations.AddField(
            model_name='group',
            name='message_updated_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
        migrations.AddField(
            model_name='message',
            name='account',
            field=models.ForeignKey(related_name='message_message', default=None, to='lineapp.Account'),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message')]),
        ),
        migrations.AlterField(
            model_name='task',
            name='completed_on',
            field=models.DateTimeField(null=True, blank=True),
        ),
    ]
