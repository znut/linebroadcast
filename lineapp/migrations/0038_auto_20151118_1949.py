# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0037_auto_20151116_2130'),
    ]

    operations = [
        migrations.AlterField(
            model_name='contact',
            name='lineid',
            field=models.CharField(max_length=33, db_index=True),
        ),
        migrations.AlterField(
            model_name='group',
            name='lineid',
            field=models.CharField(max_length=33, db_index=True),
        ),
        migrations.AlterField(
            model_name='room',
            name='lineid',
            field=models.CharField(max_length=33, db_index=True),
        ),
    ]
