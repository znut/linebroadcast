# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0020_auto_20150501_1457'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='args',
            field=jsonfield.fields.JSONField(),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='task',
            name='output',
            field=jsonfield.fields.JSONField(),
            preserve_default=True,
        ),
    ]
