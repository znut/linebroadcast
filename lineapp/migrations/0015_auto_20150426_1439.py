# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import jsonfield.fields


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0014_auto_20150426_1148'),
    ]

    operations = [
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message'), ('find_contact_by_userid', 'Find contact by userid')]),
            preserve_default=True,
        ),
    ]
