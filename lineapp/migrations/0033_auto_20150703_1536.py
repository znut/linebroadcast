# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0032_auto_20150617_2216'),
    ]

    operations = [
        migrations.AlterField(
            model_name='message',
            name='created_on',
            field=models.DateTimeField(db_index=True),
        ),
        migrations.AlterField(
            model_name='task',
            name='action',
            field=models.CharField(max_length=50, choices=[('get_messages', 'Get Messages'), ('send_message', 'Send Message'), ('read_message', 'Read Message'), ('send_image', 'Send Image'), ('find_contact_by_userid', 'Find contact by userid'), ('add_contact_by_userid', 'Add contact by userid'), ('get_blocked_contacts', 'Get blocked contacts')]),
        ),
    ]
