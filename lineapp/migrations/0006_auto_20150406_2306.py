# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0005_auto_20150326_0729'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='account',
            field=models.ForeignKey(related_name='contact_contact', blank=True, to='lineapp.Account', null=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='account',
            name='profile',
            field=models.OneToOneField(related_name='account_account', null=True, blank=True, to='lineapp.Contact'),
            preserve_default=True,
        ),
    ]
