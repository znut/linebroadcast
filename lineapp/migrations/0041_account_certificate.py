# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0040_loginhistory'),
    ]

    operations = [
        migrations.AddField(
            model_name='account',
            name='certificate',
            field=models.TextField(blank=True),
        ),
    ]
