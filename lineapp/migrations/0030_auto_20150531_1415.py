# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0029_auto_20150528_0420'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='line_read',
            field=models.BooleanField(default=False),
        ),
        migrations.AlterField(
            model_name='message',
            name='content_type',
            field=models.PositiveIntegerField(default=0, choices=[(0, 'Text'), (1, 'Image'), (2, 'Sticker')]),
        ),
    ]
