# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('lineapp', '0022_account_nickname'),
    ]

    operations = [
        migrations.AddField(
            model_name='message',
            name='content_preview',
            field=models.ImageField(null=True, upload_to='preview', blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='content_type',
            field=models.PositiveIntegerField(default=0, choices=[(0, 'Text'), (1, 'Image')]),
            preserve_default=True,
        ),
    ]
