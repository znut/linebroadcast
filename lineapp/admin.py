from django.contrib import admin
from .models import Account, Group, Contact, Room, Message, Task, ImageAttachment
from django.forms import ModelForm, PasswordInput


class AccountForm(ModelForm):
    class Meta:
        model = Account
        widgets = {
            'password': PasswordInput(),
        }
        fields = ('nickname', 'email', 'password', 'revision', 'auth_token', 'state')


class AccountAdmin(admin.ModelAdmin):
    # form = AccountForm
    fields = ('nickname', 'email', 'password', 'revision', 'auth_token', 'state', 'created_by', 'certificate')
    list_display = ('nickname', 'email', 'profile', 'revision', 'updated_on', 'state')


class MessageForm(ModelForm):
    class Meta:
        model = Message
        fields = '__all__'


class MessageAdmin(admin.ModelAdmin):
    form = MessageForm
    list_display = ('text', 'admin_thumbnail')

admin.site.register(Account, AccountAdmin)
admin.site.register(Contact)
admin.site.register(Group)
admin.site.register(Room)
admin.site.register(Message, MessageAdmin)
admin.site.register(Task)
admin.site.register(ImageAttachment)
