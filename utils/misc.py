# -*- coding: utf-8 -*-

import datetime

from django.utils import timezone
from django.utils.timesince import timesince


def import_app(app, installed_apps):
    try:
        __import__(app)
        installed_apps.append(app)
        return True
    except ImportError:
        pass
    return False


def minimal_timesince(t):
    if not t:
        return None
    # now = datetime.datetime.now()
    now     = timezone.now()
    delta   = now - t
    seconds = delta.seconds % 60
    minutes = (delta.seconds / 60) % 60
    hours   = (delta.seconds / 3600) % 60
    days    = delta.days
    if days == 0:
        if hours == 0:
            if minutes == 0:
                pass
            else:
                t = t - datetime.timedelta(seconds=-seconds)
        else:
            t = t - datetime.timedelta(minutes=-minutes)
    else:
        t = t - datetime.timedelta(hours=-hours)
    ret     = timesince(t, now)
    return ret
