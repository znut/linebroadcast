FROM python:2.7-alpine

# install bash for alpine
RUN apk add --update bash postgresql-dev
RUN apk add --update build-base zlib zlib-dev curl jpeg jpeg-dev libpng libpng-dev python-dev
RUN rm -rf /var/cache/apk/*
# fixed zlib when install pillow
ENV CFLAGS="$CFLAGS -L/lib"
ENV LIBRARY_PATH=/lib:/usr/lib

ENV PYTHONBUFFERED 1
ENV APPLICATION_ROOT /usr/src/app

RUN mkdir -p $APPLICATION_ROOT
WORKDIR $APPLICATION_ROOT

COPY . $APPLICATION_ROOT

# install requirements for alpine
RUN pip install -r $APPLICATION_ROOT/requirements.txt
