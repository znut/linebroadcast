# -*- coding: utf-8 -*-

from __future__ import unicode_literals

from django.contrib import admin
from django import forms

from .models import BotRole, BotMessageHistory


class BotRoleForm(forms.ModelForm):
    class Meta:
        model = BotRole
        widgets = {
            'allow_contacts': forms.SelectMultiple(attrs={'size': 12}),
            'allow_groups': forms.SelectMultiple(attrs={'size': 12}),
        }
        fields = '__all__'


class BotRoleAdmin(admin.ModelAdmin):
    form = BotRoleForm
    list_display = ('name', 'allow_guest', 'active', 'updated_on')
    readonly_fields = ('created_on', 'updated_on')

admin.site.register(BotRole, BotRoleAdmin)
admin.site.register(BotMessageHistory)
