from django.shortcuts import render, get_object_or_404, redirect
from actstream.models import Action
from django.contrib.auth.decorators import login_required
from django.contrib.auth.models import User
from django.contrib.contenttypes.models import ContentType
from django.core.urlresolvers import reverse


@login_required
def index(request):
    user = request.user
    content_type = ContentType.objects.get_for_model(user).pk
    return redirect(reverse('actstream_actor', kwargs={
        'content_type_id': content_type, 'object_id': user.pk}))


@login_required
def show_all(request):
    actions = Action.objects.all()
    return render(request, 'actstream/show_all.html', {
        'action_list': actions,
        'actor': None,
        'ctype': ContentType.objects.get_for_model(User)
    })
