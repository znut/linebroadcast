from django.contrib import admin
from django.conf import settings
from django.forms import ModelForm, PasswordInput
from solo.admin import SingletonModelAdmin

from .models import LineBroadcastConfiguration, SendMessage


class LineForm(ModelForm):
    class Meta:
        model = LineBroadcastConfiguration
        widgets = {
            'password': PasswordInput(),
        }
        fields = '__all__'


class LineBroadcastConfigurationAdmin(SingletonModelAdmin):
    form = LineForm


admin.site.register(LineBroadcastConfiguration, LineBroadcastConfigurationAdmin)
admin.site.register(SendMessage)
