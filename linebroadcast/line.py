# -*- coding: utf-8 -*-

import requests
import json
import time
import rsa
import re

from curve.ttypes import Provider
from lineapi.baseclient import LineBaseClient, auth_required
from lineapi.exceptions import LoginFailedException
from lineapi.curve.ttypes import TalkException
from curve import CurveThrift
from django.db import transaction
from thrift.Thrift import TException


class LineBroadcast(LineBaseClient):

    def __init__(self, account, *args, **kwargs):
        self.account  = account

        com_name     = 'WINDOWS-PC'
        auth_token   = self.account.auth_token
        super(LineBroadcast, self).__init__(email=account.email, password=account.password, com_name=com_name, auth_token=auth_token, *args, **kwargs)

    def login(self):
        """Login to LINE server."""
        try:
            result = self._login(self.account.email, self.account.password)
            if result['success']:
                self.account.auth_token = self.authToken
                self.account.save(update_fields=['auth_token'])
            return result
        except LoginFailedException as e:
            return {
                'success': False,
                'message': str(e),
            }

    def login_pincode(self):
        result = self.get_content(self.LINE_CERTIFICATE_URL)
        if not result or result == "Service unavailable":
            return False
        else:
            j = json.loads(result)
        self.verifier = j['result']['verifier']
        msg = self._loginClient.loginWithVerifierForCertificate(self.verifier)
        if msg.type == 1:
            if msg.certificate is not None:
                # with open(self.CERT_FILE, 'w') as f:
                #     f.write(msg.certificate)
                self.certificate = msg.certificate
            if msg.authToken is not None:
                self.authToken = self._headers['X-Line-Access'] = msg.authToken
                return True
            else:
                msg = "Invalid"
                self.raise_error(msg)
        else:
            msg = "Require device confirm"
            self.raise_error(msg)

    @auth_required
    def get_contacts(self):
        self.account.profile = self.profile.to_model(self.account)
        self.account.save(update_fields=['profile'])  # Save profile
        print "Getting groups ......",
        # time.sleep(1)
        self.refreshGroups()
        groups = [x for x in self.groups]
        print "successed"
        print "Getting contacts ....",
        # time.sleep(1)
        self.refreshContacts()
        contacts = [x for x in self.contacts]
        print "successed"
        with transaction.atomic():
            [x.to_model(self.account, force_update_member=True) for x in groups]
            [x.to_model(self.account) for x in contacts]
        return True

    @auth_required
    def send_message(self, lineid, text):
        try:
            from lineapi.curve.ttypes import Message
            text = text.encode('utf-8')
            message = Message(to=lineid, text=text)
            self.sendMessage(message)
            return True
        except TalkException as e:
            if e.code == 10:  # Not a member
                return False
            if e.code == 82:  # retry encrypt
                return False
            raise
        except TException as e:
            return False
        except requests.RequestException:
            return False
        except:
            raise
